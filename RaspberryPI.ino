/*
  # @author: Arnaud Joset
  #
  # This file is part of R1D2.
  #
  # R1D2 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D2 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
  #
*/



void setup_raspberrypi() {

  regs[0] = 0x00; // reg0 = execution register
  // value 0x00 = NOP - No Operation
  regs[1] = 0x00;
  regs[2] = 0x00;
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
}

void loop_raspberrypi() {

  // val 1 val 2 val 3 globals vars
  //  Serial.print("RET : ");
  //  Serial.println(ret);
  val1 += 1.52;
  val2 += 951.454;
  val3 -= 45.5;
  //ret_2 = String(val1) + String(val2) + String(val3);

  delay(500);


}

// EXPLANATIONS:
// ReceiveEvent: Raspberry PI ask to do something
// The Raspberry PI requests an action from the Arduino. It send a register and a value:
//
// i2c.write8(0x00,0x01)
//
// Register    Description
// 0x00        Execution register.
//            Place one of the accepted values here and the Arduino will execute your request.
//            This register can also be read.
// 0x01        Operation register 1.
// 0x02        Operation register 2.
//
// Register 0x00: The accepted values are:
// Value 0x00  : Do nothing
// Value 0x01  : Send the value of "ret_1" when asked. This value is mostly used.
// Value 0x02  : Send the value of "ret_2" when asked.
// Value 0x03  : Send the value of "ret_3" when asked.
// Value 0x04  : Send the value of "ret_4" when asked.

// requestEvent: Raspberry PI ask the result of the what it asked.
// print(i2c.readU8(0x00))
// Arduino read the last executed register and gives the result accordinately:
//



// This function is called when Arduino recieve some i2c data
void receiveEvent(int byteCount) {

  int byteCounter = 0;
  while (Wire.available()) {
    byteCounter += 1;
    b = Wire.read();
    if ( byteCounter == 1 ) { // Byte #1 = register number
      regIndex = b;
    }
    else {                    // Byte #2 = the value to keep in the register
      switch (regIndex) {
        case 0:
          regs[0] = b;
          // keep a copy of the last reg0 in order to process a response throught requestEvent
          lastExecReq = b;
          break;
        case 1:
          regs[1] = b;
          break;
        case 2:
          regs[2] = b;
          break;
        case 3:
          regs[3] = b;
          break;
      }
    }
  }
  flag_recieve = true;
}

// This function is called when Arduino is called to send some i2c data


// Warning - because of the way the Wire library is written, the requestEvent handler can only (successfully) do a single send.
// The reason is that each attempt to send a reply resets the internal buffer back to the start.
// Thus in your requestEvent, if you need to send multiple bytes, you should assemble them into a temporary buffer, and then send that buffer using a single Wire.write. For example:

void requestEvent() {
  switch ( regIndex ) {
    // only work with complete strings
    // we will do what is asked in register 0
    case 0x00: // read register 0
      // the response depend on the asked operation (with the help of execution register 0x00.
      switch ( lastExecReq ) {
        // Distance measurment
        case 0x01: // ask for distances and MiniMU measurments
          // data is a string with distance measurment
          //        Wire.beginTransmission(PI_ADDRESS);
          //Wire.write(0XFF, ret_1_s);
          Wire.write(a_distances, 4);
          //        Wire.endTransmission();
          break;

        case 0x02: // ask for telemetry measurment (distance etc)
          // MiniMU
          //        Wire.beginTransmission(PI_ADDRESS);
          Wire.write( 0xFF ); // 255 : Error
          //        Wire.endTransmission();
          break;

        // MiniMu again
        case 0x03: // ask for telemetry measurment (distance etc)
          //        Wire.beginTransmission(PI_ADDRESS);
         Wire.write( 0xFF ); // 255 : Error
          //         Wire.endTransmission();
          break;

        // MiniMu again
        case 0x04: // ask for telemetry measurment (distance etc)
          //         Wire.beginTransmission(PI_ADDRESS);
          Wire.write( 0xFF ); // 255 : Error
          //         Wire.endTransmission();
          break;


        default:
          //          Wire.beginTransmission(PI_ADDRESS);
          Wire.write( 0xFF ); // 255 : Error
          //          Wire.endTransmission();
      }
      break;

    default: // read another register
      //    Wire.beginTransmission(PI_ADDRESS);
      Wire.write( 0xFF ); // 255 :  Error
      //     Wire.endTransmission();
  }


  flag_request = true;


}

