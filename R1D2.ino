/*
  # @author: Arnaud Joset
  #
  # This file is part of R1D2.
  #
  # R1D2 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D2 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

#include <Wire.h>


long timer = 0; //general purpuse timer 
long timer_old; 
long timer24 = 0; //Second timer used to print values
unsigned int counter = 0;

// # I2C RASPBERRY PI ###############################################
// ############################################################

#define SLAVE_ADDRESS 0x12

// Register the registers
byte regs[4];
int regIndex = 0; // Current register
// last executed register
byte lastExecReq = 0x00;
const int buf_size = 30;
float val1 = 0, val2 = 0, val3 = 0;

volatile bool flag_recieve = false;
volatile bool flag_request = false;
volatile byte b;
byte a_distances[4];


// # R1D2 ###############################################
// ############################################################

void setup() {
  Serial.begin(9600);
  setup_motors();
  setup_distance();
  setup_raspberrypi();
}

void loop() {

  //loop_minimu();
  //loop_distances();
  //loop_raspberrypi();


  if ((millis() - timer) >= 1000 ) // Main loop runs at XXHz
  {
    


    counter++;
    timer_old = timer;
    timer = millis();
    if (timer > timer_old){
      // do something
      Serial.println("Big timer");
  
    }

    if (counter > 5)  // Read compass data at 10Hz... (5 loop runs)
    {
     // do something
      Serial.println("every 5 loops");
      counter = 0;
      Serial.println(a_distances[0]);
    }

   
  

    // Distance detectors:
    // 0 : front
    // 1 : back
    // 2 : left
    // 3 : back

    int detector = 0;
    int distance_front = distances(detector);
    a_distances[0] = (byte) distance_front;
    a_distances[1] = 255;
    a_distances[2] = 255;
    a_distances[3] = 255;
 
 
    if (flag_request == true) {
 /*     Serial.print("#######  request : ");
      Serial.print("index : ");
      Serial.print(regIndex);
      String test = "";
      test = " TEST EULER " + String(roll) + " " + String(pitch) + " " + String(yaw);
      Serial.println(test);
      Serial.println(" ret : ");
      Serial.print((char*)ret_1);
      Serial.print(" size = ");
      Serial.println(ret_1_s);
      Serial.print((char*)ret_2);
      Serial.print(" size = ");
      Serial.println(ret_2_s);
      Serial.print((char*)ret_3);
      Serial.print(" size = ");
      Serial.println(ret_3_s);
      Serial.print((char*)ret_4);
      Serial.print(" size = ");
      Serial.println(ret_4_s);
      Serial.print(" last exec : ");
      Serial.println(lastExecReq);
*/
      flag_request = false;

  
      delay(200);

    }
    
    if (flag_recieve == true) {
  /*
      Serial.print("recieve : ");
      Serial.print("index : ");
      Serial.print(regIndex);
      Serial.print(" last exec : ");
      Serial.print(lastExecReq);
      Serial.print(" command : ");
      Serial.println(b);
 */
      flag_recieve = false;
      delay(200);

    }




  }

}

