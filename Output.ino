/*
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#
*/


void printdata_minimu(void)
{
  Serial.print("=");

#if PRINT_EULER == 1
  Serial.print("ANG:");
  Serial.print(ToDeg(roll));
  Serial.print(",");
  Serial.print(ToDeg(pitch));
  Serial.print(",");
  Serial.print(ToDeg(yaw));
#endif
#if PRINT_ANALOGS==1
  Serial.print(",AN:");
  Serial.print(AN[0]);  //(int)read_adc(0)
  Serial.print(",");
  Serial.print(AN[1]);
  Serial.print(",");
  Serial.print(AN[2]);
  Serial.print(",");
  Serial.print(AN[3]);
  Serial.print (",");
  Serial.print(AN[4]);
  Serial.print (",");
  Serial.print(AN[5]);
  Serial.print(",");
  Serial.print(c_magnetom_x);
  Serial.print (",");
  Serial.print(c_magnetom_y);
  Serial.print (",");
  Serial.print(c_magnetom_z);
#endif
  /*#if PRINT_DCM == 1
    Serial.print (",DCM:");
    Serial.print(convert_to_dec(DCM_Matrix[0][0]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[0][1]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[0][2]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[1][0]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[1][1]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[1][2]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[2][0]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[2][1]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[2][2]));
    #endif*/
  Serial.println();

}

long convert_to_dec(float x)
{
  return x * 10000000;
}


void minimu_out() {


#if PRINT_EULER == 1

  minimu_ret_a = String("") ;
  minimu_ret_a += String(ToDeg(roll)) + ";";
  minimu_ret_a += String(ToDeg(pitch)) + ";";
  minimu_ret_a += String(ToDeg(yaw)) + "%";
#endif
#if PRINT_ANALOGS==1
  minimu_ret_b = String("") ;
  minimu_ret_b += String(AN[0]) + ";";
  minimu_ret_b += String(AN[1]) + ";";
  minimu_ret_b += String(AN[2]) + ";";
  minimu_ret_b+= String(AN[3]) + ";";
  minimu_ret_b += String(AN[4]) + ";";
  minimu_ret_b += String(AN[5]) + "%";
  // B is symbol of magnetic field
  minimu_ret_c = String("");
  minimu_ret_c += String(c_magnetom_x) + ";";
  minimu_ret_c += String(c_magnetom_y) + ";";
  minimu_ret_c += String(c_magnetom_z) + "%";

#endif

}

