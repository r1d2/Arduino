#/*
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#
*/

#include <NewPing.h>

#define PIN_FRONT  53  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define PIN_BACK  54  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define PIN_LEFT  55  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define PIN_RIGHT  56  // Arduino pin tied to trigger pin on the ultrasonic sensor.
// distance is a byte. MAXIMUM value = 254, 255 = error
#define MAX_DISTANCE 254 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar_front(PIN_FRONT, PIN_FRONT, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_back(PIN_BACK, PIN_BACK, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_left(PIN_LEFT, PIN_LEFT, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_right(PIN_RIGHT, PIN_RIGHT, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup_distance()
{

}


void loop_distances() {
  delay(50);                     // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
  Serial.print("Ping: ");
  //Serial.print(sonar.ping_cm()); // Send ping, get distance in cm and print result (0 = outside set distance range)
  int mu = sonar_front.ping_median();
  int d = sonar_front.convert_cm(mu);
  Serial.print(d);
  Serial.println("cm");

}


int distances(int detector) {
  int distance;
  int mu;

  switch (detector) {
    case 0:
      mu = sonar_front.ping_median();
      distance = sonar_front.convert_cm(mu);
      break;
    case 1:
      mu = sonar_back.ping_median();
      distance = sonar_back.convert_cm(mu);
      break;
    case 2:
      mu = sonar_left.ping_median();
      distance = sonar_left.convert_cm(mu);
      break;
    case 3:
      mu = sonar_right.ping_median();
      distance = sonar_right.convert_cm(mu);
      break;
  }
  return distance;
}

