/*
# @author: Arnaud Joset
#
# This file is part of R1D2.
#
# R1D2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# R1D2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with R1D2.  If not, see <http://www.gnu.org/licenses/>.
#
*/

#include <Encoder.h>

// Change these two numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
Encoder myEnc(2, 3);
//   avoid using pins with LEDs attached

int pwm_pin1 = 11;
int dir_pin1 = 53;

int pwm_pin2 = 10;
int dir_pin2 = 31;
bool dir = HIGH;

void setup_motors() {

  pinMode(dir_pin1, OUTPUT);
  pinMode(pwm_pin1, OUTPUT);
  pinMode(dir_pin2, OUTPUT);
  pinMode(pwm_pin2, OUTPUT);
  Serial.println("start");                // a personal quirk

}

void loop_motors() {
  // do some stuff here - the joy of interrupts is that they take care of themselves

  int i = 150;
  digitalWrite(dir_pin1, dir);
  digitalWrite(dir_pin2, dir);

  // long newPosition = myEnc.read();



  do
  {
    analogWrite(pwm_pin1, i);
    analogWrite(pwm_pin2, i);
    /*   Serial.print("DIR : ");
       Serial.print("HIGH");
       Serial.print(" i = ");
       Serial.print(i);
       Serial.print(" Encoder pos : ");
       Serial.println (newPosition); */
    delay(500);
    i += 50;
  } while (i < 260);

  analogWrite(pwm_pin1, 0);
  analogWrite(pwm_pin2, 0);

  delay(1500);
  dir = !dir;
}
